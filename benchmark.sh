#!/bin/bash

printf "Starting benchmark\n"

for pic in "20x20.jpg" "50x50.jpg" "100x100.jpg" "200x200.jpg"
do
    printf "\n\n\nUsing: $pic\n\n"

    printf "Sigma=20 Maxit=10 Tol=1e-4\n"
    \time -f "%e" python3 main.py --file "src/$pic" --sigma 20 --maxit 10 --tol 1e-4 >/dev/null
    printf "Sigma=50 Maxit=30 Tol=1e-4\n"
    \time -f "%e" python3 main.py --file "src/$pic" --sigma 50 --maxit 30 --tol 1e-4 >/dev/null
    printf "Sigma=20 Maxit=10 Tol=1e-2\n"
    \time -f "%e" python3 main.py --file "src/$pic" --sigma 20 --maxit 10 --tol 1e-2 >/dev/null
    printf "Sigma=50 Maxit=30 Tol=1e-2\n"
    \time -f "%e" python3 main.py --file "src/$pic" --sigma 50 --maxit 30 --tol 1e-2 >/dev/null
done