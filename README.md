# TV Denoising

Simple implementation of TV denoising in python.
I once did a semester project where we had to denoise 1D signals using different minimization methods (e.g. Lasso, least square, etc.).
This is an adaptation to 2D signals, i.e. images, using specifically TV denoising.

[ Link to a paper on TV denoising ](https://www.researchgate.net/publication/326824759_Two-Dimensional_Total_Variation_Norm_Constrained_Deconvolution_Beamforming_Algorithm_for_Acoustic_Source_Identification
)


# Utilisation
At the moment the program only support grayscale denoising, meaning it will convert whichever image we give it to gray.

By default it will load a small 20x20 image, add noise to it and denoise it:

    python3 main.py

You can change the default file, noise added, max number of iteration, and tolerance in the algorithm with the corresponding arguments: `--file --sigma --maxit --tol`.
E.g.

    python3 main.py --file "~/foo.jpg" --sigma 45 --maxit 2

will load the file `~/foo.jpg`, add gaussian noise with std deviation 45 to it and use a maximum of 2 iterations.



# Small benchmark
I used the images given in the src folder, i.e.

<p style="text-align:center">
<img src="src/200x200.jpg" alt="200 x 200 image">
<figcaption style="text-align:center">The 200x200 version of the image </figcaption>
</p>


|Parameters| 20x20.jpg | 50x50.jpg | 100x100.jpg | 200x200.jpg |
|:-:|:---------:|:---------:|:-----------:|:-----------:|
|Sigma=20, Maxit=10, Tol=1e-4| &#8764;5s | &#8764;30s | &#8764;3m | &#8764;30m |
|Sigma=50, Maxit=30, Tol=1e-4| &#8764;13s | &#8764;1m35s | &#8764;9m30s | &#10007; |
|Sigma=20, Maxit=10, Tol=1e-2| &#8764;5s | &#8764;30s | &#10007; | &#10007; |
|Sigma=50, Maxit=30, Tol=1e-2| &#8764;5s | &#8764;1m5s | &#10007; | &#10007; |

Where ( &#10007; ) means I have not bothered to try (yet).


If you want to try to benchmark for yourself, when in the correct directory simply use:

    chmod +x benchmark.sh && ./benchmark.sh

# Sample results
Used the 100x100 version, with parameters Sigma=20 and Maxit=30.

<p style="text-align:center">
<img src="results/sampleGray.jpg">
<img src="results/sampleNoisy.jpg">
<img src="results/sampleDenoised.jpg">
</p>

# TODO

- [ ] For large images use chunks rendering with multithreading
- [X] Small parser
- [ ] Add a simple C++ version of the script
- [X] Make max_it actually do something by exiting early in case objective function doesn't change much