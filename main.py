#from tkinter.tix import MAX
import matplotlib.pyplot as plt
import scipy.sparse as sp
import numpy as np
import numpy.linalg as LA
import time
import cv2

cv2.setRNGSeed( int(time.time()))
"""
Link to paper about TV-denoising
https://www.researchgate.net/publication/326824759_Two-Dimensional_Total_Variation_Norm_Constrained_Deconvolution_Beamforming_Algorithm_for_Acoustic_Source_Identification
"""

#Object Image will store all tools to process it
class Image:
    def __init__(self, path):
        print(' -- Importing file')
        self.color = cv2.imread(path)
        self.gray = cv2.cvtColor(self.color, cv2.COLOR_BGR2GRAY)

        self.shape = np.shape(self.gray)
        self.dim = np.size(self.gray.flatten())

    def Normalised(self):
        return (self.noisy.astype('float64')/255).flatten()

    def add_noise(self, mean, sigma):
        print(' -- Adding noise to image')
        noise = np.empty(self.shape)
        cv2.randn(noise, mean, sigma)
        self.noisy = cv2.add(self.gray, noise, dtype=cv2.CV_8U)

    def save_noisy(self, filename='IMG', path='results'):
        ext = '.jpg'
        full = path + '/' + filename
        print(f' -- Saving noisy image to {full + "Noisy" + ext}')
        cv2.imwrite(full+'Noisy'+ext, self.noisy)

    def save_denoised(self, filename='IMG', path='results'):
        if self.denoised is None: raise Exception('Have not denoised the image yet')
        ext = '.jpg'
        full = path + '/' + filename
        print(f' -- Saving denoised image to {full + "Denoised" + ext}')
        cv2.imwrite(full+'Denoised'+ext, self.denoised)

    def save_gray(self, filename='IMG', path='results'):
        ext = '.jpg'
        full = path + '/' + filename
        print(f' -- Saving gray image to {full + "Gray" + ext}')
        cv2.imwrite(full+'Gray'+ext, self.gray)

    def denoise(self, LAMBDA=0.15, max_it=100, parts=None,tol=1e-4):
        if parts is None:
            print(' -- Starting to denoise')
            img_res = self.denoising_algo(self.Normalised(), self.shape, LAMBDA, max_it,tol=tol)
            self.denoised = img_res.reshape(self.shape)

    #Compute the TVDenoising for given image (Given as a flat array)
    def denoising_algo(self, arr, img_shape, LAMBDA, max_it, solo=True, tol=1e-4):

        #First let's create the matrix for finite differences
        n = len(arr)
        D1 = sp.lil_matrix((n-img_shape[1], n), dtype=np.int8)
        D2 = sp.lil_matrix((n, n), dtype=np.int8)
        D1.setdiag(-1)
        D1.setdiag(1, img_shape[1])
        D2.setdiag(-1)
        D2.setdiag(1, 1)
        D2[( np.arange(0, img_shape[1] + n, img_shape[1]) - 1)[1:] ] = 0
        D = sp.csr_matrix(sp.vstack((D1, D2)))
        D.eliminate_zeros()
        del D1, D2

        #Then implement the dual optimization algorithm
        dim = D.shape[0]
        v = np.zeros(shape=(dim,), dtype='float64')
        #This get's back the original variables form the dual reformulation ones
        def Get_Original(v): return ( arr - D.T.dot(v)/2 )

        # Is used to check stopping criterion
        def Dual_gap(v_k):
            f_k = Get_Original(v_k)
            f_cost = LA.norm(arr - f_k)**2 + LAMBDA * LA.norm(D.dot(f_k), 1)

            g_cost = LA.norm(D.transpose().dot(v_k), 2)**2
            g_cost = np.dot(arr.transpose(), D.transpose().dot(v_k)) - (g_cost/4)

            return (f_cost - g_cost)


        #Let's make a progress bar if the image is processed individualy, otherwise dont
        if solo:
            from tqdm import tqdm
            pbar = tqdm(total=max_it*dim)

        it = 0
        old_gap = np.inf
        while ( it < max_it ):
            for i in range(dim):
                R = 2*arr - D[np.delete(np.arange(dim), i)].T.dot(np.delete(v, i))
                #r = 2*arr - np.dot(np.delete(X, i,1), np.delete(v, i))
                z = D[i,:].dot(D[i,:].T)[0,0]
                if z != 0:
                    q = D[i,:].dot(R)/z
                else:
                    q = 0

                v[i] = np.clip(q, -LAMBDA, LAMBDA)
                if solo: pbar.update(1)


            it += 1

            # Check the changes in the update function every few iterations
            if (it % 2 == 0):
                tmp = Dual_gap(v)
                change = np.abs(tmp - old_gap)
                if (change < tol):
                    print(' -- Exited with dual gap')
                    it = max_it + 1
                else:
                    old_gap = tmp
                
        return Get_Original(v)*255




if __name__ == '__main__':
    import sys, argparse

    # Parse the args
    parser = argparse.ArgumentParser(description='Simple implementation of TV Denoising.')
    parser.add_argument('--file', type=str, help='Filepath')
    parser.add_argument('--maxit', type=int, help='Max number of iterations')
    parser.add_argument('--sigma', type=int, help='Std deviation of noise added')
    parser.add_argument('--tol', type=float, help='Tolerance before exiting algorithm')

    args = parser.parse_args()

    file = args.file if args.file else 'src/20x20.jpg'
    sigma = args.sigma if args.sigma else 20
    maxit = args.maxit if args.maxit else 20
    tolerance = args.tol if args.tol else 1e-4

    image = Image(file)
    image.add_noise(0, sigma)
    image.denoise(max_it=maxit,tol=tolerance)
    image.save_denoised()
    image.save_noisy()
    image.save_gray()
